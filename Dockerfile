from registry.gitlab.com/emulation-as-a-service/emulators/emulators-base-ng

LABEL "EAAS_EMULATOR_TYPE"="pcem"
LABEL "EAAS_EMULATOR_VERSION"="git+eaas-07102019"


RUN mkdir /pcem
RUN curl -o /pcem/PCemV15Linux.tar.gz https://pcem-emulator.co.uk/files/PCemV15Linux.tar.gz
RUN tar xvzf /pcem/PCemV15Linux.tar.gz -C /pcem/
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes libalut-dev \
libwxgtk3.0-dev \
libsdl2-dev libsdl2-2.0-0 -y;
WORKDIR /pcem/
RUN ./configure && make && make install
RUN DEBIAN_FRONTEND=noninteractive apt-get install xterm -y
COPY .pcem /root/.pcem
RUN rm -rf /pcem
COPY wrapper.sh /wrapper.sh
